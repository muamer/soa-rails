require 'rubygems'
require 'bundler/setup'

require 'active_record'
require 'sinatra'
#require "#{File.dirname(__FILE__)}/models/modelname"

# setting up our environment
env_index = ARGV.index("-e")
env_arg = ARGV[env_index + 1] if env_index
env = env_arg || ENV["SINATRA_ENV"] || "development"
databases = YAML.load_file("config/database.yml")
ActiveRecord::Base.establish_connection(databases[env])

# the HTTP entry points to our service

get '/api/v1/hello' do
	"Hello World!"
end
